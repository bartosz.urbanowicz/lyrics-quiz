# dev setup:

## django:

```bash
cd lyrics_quiz
```
open virtual environment

```bash
pipenv shell
```
start the server

```bash
python manage.py runserver
```

## react:

```bash
cd .\lyrics_quiz\frontend\
```

start the website

```bash
npm run dev
```

# changing the model:

make chages to models

```bash
python manage.py makemigrations
```

apply to database

```bash
python manage.py migrate
```
