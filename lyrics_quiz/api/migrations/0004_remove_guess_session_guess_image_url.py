# Generated by Django 4.1.7 on 2023-05-11 16:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0003_alter_guess_session'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='guess',
            name='session',
        ),
        migrations.AddField(
            model_name='guess',
            name='image_url',
            field=models.CharField(default='', max_length=500),
        ),
    ]
