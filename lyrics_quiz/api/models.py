from django.db import models

# Create your models here.

class Guess(models.Model):
    name = models.CharField(max_length=100, default = "")
    song_title = models.CharField(max_length=100, default = "")
    lyric = models.CharField(max_length=200, default = "")
    guess = models.CharField(max_length=100, default = "")
    result = models.BooleanField(default=False)
    image_url = models.CharField(max_length=500, default = "")

    def _str_(self):
        return self.name