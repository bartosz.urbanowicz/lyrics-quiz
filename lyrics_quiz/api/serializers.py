from rest_framework import serializers
from .models import Guess

class GuessSerializer(serializers.ModelSerializer):
    class Meta:
        model = Guess
        fields = ('id', 'name', 'song_title', 'lyric', 'guess', 'result', 'image_url')

class CreateGuessSerializer(serializers.ModelSerializer):
    class Meta:
        model = Guess
        fields = ('name', )