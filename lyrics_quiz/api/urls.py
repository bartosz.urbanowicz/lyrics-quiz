from django.urls import path
from .views import GuessView, CreateGuessView

urlpatterns = [
    path('guess', GuessView.as_view()),
    path('create-guess', CreateGuessView.as_view())
]
