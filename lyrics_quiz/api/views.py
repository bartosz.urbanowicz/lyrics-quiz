from django.shortcuts import render
from rest_framework import generics, status
from .serializers import GuessSerializer, CreateGuessSerializer
from .models import Guess
from rest_framework.views import APIView
from rest_framework.response import Response
from lyricsgenius import Genius
import requests
import random
import re

# Create your views here.

class GuessView(generics.CreateAPIView):
    queryset = Guess.objects.all()
    serializer_class = GuessSerializer

class CreateGuessView(APIView):
    serializer_class = CreateGuessSerializer

    def post(self, request, format=None):
        
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            client_access_token = 'RTjPqaCSOni5O3ph6xW5UZL4sytrox9d5nPW-t2XcOUttO8QvXtMwy1tw2HDgjjC'
            def clean_up_lyrics(lyrics):
                bad_lyrics_regex = ["^$", "^\[.*\]$", "^\(.*\)$", "Lyrics", "You might also like"]

                i = 0
                while True:
                    for j in range(len(bad_lyrics_regex)):
                        if re.search(bad_lyrics_regex[j], lyrics[i]) != None:
                            lyrics.pop(i)
                            i -= 1
                    i += 1
                    if i == len(lyrics):
                        break
                return lyrics

            genius = Genius(client_access_token)

            name = serializer.data.get('name')

            # songs_scrapped = 3

            # name = serializer.data.get('name')
            # artist = genius.search_artist(name, max_songs=songs_scrapped, include_features=False, sort="popularity", get_full_info=False)

            # random_song_number = random.randint(0, songs_scrapped - 1)
            # song_title = artist.songs[random_song_number].title

            # lyrics = artist.songs[random_song_number].lyrics.split("\n")
            # lyrics = clean_up_lyrics(lyrics)

            # random_lyric_number = random.randint(0, len(lyrics) - 1)
            # lyric = lyrics[random_lyric_number]

            # image_url = artist.image_url

            artist = genius.search_artists(name)['sections'][0]['hits'][0]['result']
            artist_id = artist['id']
            name = artist['name']
            image_url = artist['image_url']

            songs = genius.artist_songs(artist_id, per_page=50, sort='popularity')['songs']
            random_song_number = random.randint(0, len(songs) - 1)
            lyrics = genius.lyrics(songs[random_song_number]['id']).split("\n")
            lyrics = clean_up_lyrics(lyrics)

            song_title = songs[random_song_number]['title']

            random_lyric_number = random.randint(0, len(lyrics) - 1)
            lyric = lyrics[random_lyric_number]

            guess = Guess(name=name, song_title=song_title, lyric=lyric, image_url = image_url)
            guess.save()
            return Response(GuessSerializer(guess).data, status=status.HTTP_201_CREATED)