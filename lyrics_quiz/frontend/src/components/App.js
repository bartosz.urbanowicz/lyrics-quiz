import React, { Component } from "react";
import { createRoot } from 'react-dom/client';
import ArtistSearchPage from "./ArtistSearchPage";

export default class App extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <ArtistSearchPage/>
            </div>
        );
    }
}

const appDiv = document.getElementById("app");
const root = createRoot(appDiv)
root.render(<App />)