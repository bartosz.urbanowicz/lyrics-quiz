import React, { Component } from "react";
import "./ArtistSearchPage.css";
import Loading from "./Loading";
import EnterGuessPage from "./EnterGuessPage";

export default class ArtistSearchPage extends Component{
    constructor(props){
        super(props);
        this.state = {name: "", data: "", loading: false, guess: false};

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        this.setState({
            name: e.target.value
        });
    }

    handleSubmit(e) {
        console.log('name submitted: ' + this.state.name)

        this.setState({
            loading: true
        })

        const requestOptions = {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({name: this.state.name}),
        };
        
        fetch('api/create-guess', requestOptions).then((response) =>
         response.json()
         ).then((data) => this.setState({
            loading: false,
            guess: true,
            data: data
        })
        );
        e.preventDefault();
    }   

    render() {
        return this.state.loading ? <Loading/> : (this.state.guess ? <EnterGuessPage 
        title={this.state.data.song_title}
        lyric={this.state.data.lyric} 
        image_url={this.state.data.image_url} 
         /> : <div>
            <form onSubmit={this.handleSubmit}>
                <input type="text" placeholder="search for artist" spellCheck="false" value={this.state.value} onChange={this.handleChange}/>
            </form>
        </div>);
    }
}