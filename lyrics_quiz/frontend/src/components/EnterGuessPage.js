import React, { Component } from "react";
import FinalPage from "./FinalPAge";

export default class EnterGuessPage extends Component{
    constructor(props){
        super(props);
        this.state = {guess: "", correct: false, submitted: false}

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        this.setState({
            guess: e.target.value
        });
    }

    handleSubmit(e) {
        console.log('guess submitted: ' + this.state.guess)
        this.setState({
            correct: (this.state.guess.toLowerCase() === this.props.title.toLowerCase()),
            submitted: true
        })
        console.log(this.state.correct ? "correct!" : `wrong! correct title is ${this.props.title}`);
        e.preventDefault();
    }   

    render() {
        return this.state.submitted ?
        <FinalPage title={this.props.title} correct={this.state.correct}/> :
        <div>
            <div className="img">
                <img src={this.props.image_url} alt="artist's profile picture"></img>
            </div>
            <h1>{this.props.lyric}</h1>
            <div className="form">
                <form onSubmit={this.handleSubmit}>
                    <input type="text" placeholder="enter the song title" spellCheck="false" value={this.state.value} onChange={this.handleChange}/>
                </form>
            </div>
        </div>
        
    }
}