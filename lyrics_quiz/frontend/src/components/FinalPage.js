import React, { Component } from "react";
import { render } from "react-dom";

export default class FinalPage extends Component {
    constructor(props) {
        super(props);
    }

    refreshPage(e) {
        window.location.reload(false);
    }

    render() {
        return (
            this.props.correct ?
            <div>
                <h1 style={{color: "green"}}>
                    Correct!
                </h1>
                <button onClick={this.refreshPage}>
                    Try again
                </button>
            </div> :
            <div>
                <h1>
                    <span style={{color: "red"}}>Wrong! </span>Correct title is "{this.props.title}"
                </h1>
                <div className="button">
                    <button onClick={this.refreshPage}>Try again</button>
                </div>
            </div>
        );
    }
}
